package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        this.name = name;
        if (salary < 100000){
            throw new IllegalArgumentException("Salary cannot go lower than 100000");
        }
        this.salary = salary;
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
