package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        this.name = name;
        if (salary < 200000){
            throw new IllegalArgumentException("Salary cannot go lower than 200000");
        }
        this.salary = salary;
        this.role = "CEO";
    }

    @Override
    public double getSalary() {
        return salary;
        //TODO Implement
    }
}
