package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class BigBellyBurger {

    public static void main(String[] args){
        Food burger = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        burger = FillingDecorator.BEEF_MEAT.addFillingToBread(burger);
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);
        System.out.println("ITSSS AAAAAAAA " + burger.getDescription());
        System.out.println("PREPARE YOUR WALLET " + burger.cost());
    }
}