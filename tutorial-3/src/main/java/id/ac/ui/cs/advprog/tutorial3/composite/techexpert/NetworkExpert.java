package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        this.name = name;
        if (salary < 70000){
            throw new IllegalArgumentException("Salary cannot go lower than 70000");
        }
        this.salary = salary;
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
