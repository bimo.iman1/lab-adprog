package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Filling {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
        description = food.getDescription() + ", adding cucumber";
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.4;
    }
}
