package id.ac.ui.cs.advprog.tutorial1.strategy;
import id.ac.ui.cs.advprog.tutorial1.strategy.Duck;

public class MallardDuck extends Duck {
    // TODO Complete me!
	public MallardDuck(){
		this.setFlyBehavior(new FlyWithWings());
		this.setQuackBehavior(new Quack());
	}
	
	public void display(){
		System.out.println("IM QUACKQUACK ALIVE!");
	}
}
